﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace WarCardGame
{
    class Program
    {
        // The objects for the two players
        static Player humanPlayer, cpuPlayer;
        // Deck that contains all of the cards in the deck
        static List<Card> mainDeck;

        // The spacing used for each of the player columns
        const int columnSpacing = 20;
        // The amount of time in ms used for pauses between lines
        const int pauseTiming = 300;

        static void Main(string[] args)
        {
            // Whether or not to play the game, for multiple playthroughs
            bool playingGame = true;
            // Whether or not the current game is over
            bool gameOver;

            // Set output encoding so the console can show the card suit icons
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            // Get the player's name and initialize the objects
            Console.WriteLine("Please type your name and press Enter:");
            humanPlayer = new Player(Console.ReadLine());
            cpuPlayer = new Player("CPU");

            // Initialize the main deck
            InitializeDeck();


            // Main loop for the program. One loop is one full game of WAR
            do
            {
                // The game has just started, so make sure the game isn't set to over
                gameOver = false;

                // Initialize the game by setting up the player decks
                DealCards();


                // The main loop for the game. One loop is a full turn
                while (!gameOver)
                {
                    // Clear the console for the new turn
                    Console.Clear();

                    // Output the header (names and card count)
                    Console.WriteLine("{0,-" + columnSpacing + "}|{1," + columnSpacing + "}",
                        humanPlayer.PlayerName, cpuPlayer.PlayerName);
                    UpdateCardCount(true);
                    Console.WriteLine(new string(' ', columnSpacing) + "|" + new string(' ', columnSpacing));

                    // Grab the players' cards
                    Card humanCard = humanPlayer.PlayCard();
                    Card cpuCard = cpuPlayer.PlayCard();

                    // Add the cards to a temporary list so they can be added to the bottom of the winning player's deck
                    List<Card> currentHand = new List<Card>();
                    currentHand.Add(humanCard);
                    currentHand.Add(cpuCard);

                    // Show the players' cards
                    DisplayCards(humanCard, cpuCard);


                    // Figure out who won
                    bool humanWinner;
                    bool gameTie = false;

                    if (humanCard.GetCardValue() > cpuCard.GetCardValue())
                        humanWinner = true;
                    else if (humanCard.GetCardValue() < cpuCard.GetCardValue())
                        humanWinner = false;
                    else
                    {
                        // The cards are tied. WAR!!!
                        // Add a Sleep call for timing
                        Thread.Sleep(pauseTiming);

                        DisplayCenteredMessage("WAR!!!", '-');
                        humanWinner = HandleWar(currentHand, ref gameTie);
                    }


                    // Handle the player winning the hand
                    // If it's a game tie no one won the hand
                    if (!gameTie)
                    {
                        string winnerName;

                        if (humanWinner)
                        {
                            // Add the cards to the player's deck
                            humanPlayer.AddCards(currentHand);
                            winnerName = humanPlayer.PlayerName;
                        }
                        else
                        {
                            // Add the cards to the player's deck
                            cpuPlayer.AddCards(currentHand);
                            winnerName = cpuPlayer.PlayerName;
                        }

                        // Add a Sleep call for timing
                        Thread.Sleep(pauseTiming);

                        DisplayCenteredMessage(winnerName + " wins the hand!");
                        Console.WriteLine();
                    }

                    // Add a Sleep call for timing
                    Thread.Sleep(pauseTiming);

                    // Check whether the game is over
                    if (gameTie)
                    {
                        // Game was tied
                        gameOver = true;
                        Console.WriteLine("Game over! It's a draw.");
                    }
                    else if (!humanPlayer.HasCards)
                    {
                        // Human player out of cards
                        gameOver = true;
                        Console.WriteLine("Game over! " + cpuPlayer.PlayerName + " wins!");
                    }
                    else if (!cpuPlayer.HasCards)
                    {
                        // CPU player out of cards
                        gameOver = true;
                        Console.WriteLine("Game over! " + humanPlayer.PlayerName + " wins!");
                    }
                    else
                    {
                        // Both players are still in, wait for the player to continue
                        Console.WriteLine("Press any key to start the next hand...");

                        // Run this loop to remove all pending key presses (since pressing during a pause will create a new key press)
                        while (Console.KeyAvailable)
                        {
                            Console.ReadKey(true);
                        }

                        Console.ReadKey(true);
                    }
                }

                // Ask the player if they want to continue
                string answer = "";

                Console.WriteLine();
                Console.Write("Do you want to play again? (Y/N): ");
                answer = Console.ReadLine().ToUpper();

                // Make sure the input is valid
                while (answer != "Y" && answer != "N")
                {
                    Console.Write("Please enter either Y or N and press enter: ");
                    answer = Console.ReadLine().ToUpper();
                }

                // If player doesn't want to play again, set the bool to exit the loop
                if (answer == "N")
                    playingGame = false;

            } while (playingGame);
        }


        /// <summary>
        /// Generate all of the cards in the deck
        /// </summary>
        static void InitializeDeck()
        {
            mainDeck = new List<Card>();

            // Loop through the suits and the values to create one of each card
            foreach (CardSuit suit in Enum.GetValues(typeof(CardSuit)))
            {
                foreach (CardValue value in Enum.GetValues(typeof(CardValue)))
                {
                    mainDeck.Add(new Card(value, suit));
                }
            }
        }

        /// <summary>
        /// Clear the players' current decks and deal new decks to them
        /// </summary>
        static void DealCards()
        {
            // Make sure both the players are initialized, just to be safe
            if (humanPlayer == null || cpuPlayer == null)
                return;

            // Clear both decks before starting
            humanPlayer.ClearDeck();
            cpuPlayer.ClearDeck();


            // Copy the main deck to use for the deal
            List<Card> dealCards = new List<Card>(mainDeck);

            // Create a boolean to flip back and forth to decide which player is getting the current card
            bool dealToHumanPlayer = true;
            // Create a Random object to use for the loop
            Random rng = new Random();
            // Card object to store the newly chosen card
            Card newCard;
            // Variable to hold the randomly chosen integer
            int randomNumber;


            // Loop and choose a card at random for the current player
            while (dealCards.Count > 0)
            {
                // Generate a random number within the bounds of the list
                randomNumber = rng.Next(0, dealCards.Count);

                // Grab that card from the deck
                newCard = dealCards[randomNumber];
                dealCards.RemoveAt(randomNumber);

                // Add the card to the current player's deck
                if (dealToHumanPlayer)
                    humanPlayer.AddCard(newCard);
                else
                    cpuPlayer.AddCard(newCard);

                // Flip the boolean to the other player
                dealToHumanPlayer = !dealToHumanPlayer;
            }
        }

        /// <summary>
        /// Print the card count to the screen
        /// </summary>
        /// <param name="firstDraw">Whether this is the first time the count's being drawn since the last clear</param>
        static void UpdateCardCount(bool firstDraw = false)
        {
            // Get the current cursor position
            int top = Console.CursorTop;
            int left = Console.CursorLeft;

            // Change the cursor position if this isn't the first draw
            if (!firstDraw)
                Console.SetCursorPosition(0, 1);

            Console.WriteLine("{0,-" + columnSpacing + "}|{1," + columnSpacing + "}",
                "Cards: " + (humanPlayer.CardCount < 10 ? " " : "") + humanPlayer.CardCount,
                "Cards: " + (cpuPlayer.CardCount < 10 ? " " : "") + cpuPlayer.CardCount);

            // Change the cursor position back if this isn't the first draw
            if (!firstDraw)
                Console.SetCursorPosition(left, top);
        }

        /// <summary>
        /// Displays the formatted string showing card values
        /// </summary>
        /// <param name="humanCard"></param>
        /// <param name="cpuCard"></param>
        static void DisplayCards(Card humanCard, Card cpuCard, bool humanFaceDown = false, bool cpuFaceDown = false)
        {
            string humanCardLabel, cpuCardLabel;

            // Determine the card labels based on whether or not ther is a card and if it's face down
            if (humanCard == null)
                humanCardLabel = "";
            else if (humanFaceDown)
                humanCardLabel = "--";
            else
                humanCardLabel = humanCard.GetCardLabel();

            if (cpuCard == null)
                cpuCardLabel = "";
            else if (cpuFaceDown)
                cpuCardLabel = "--";
            else
                cpuCardLabel = cpuCard.GetCardLabel();

            // Add a Sleep call for timing
            Thread.Sleep(pauseTiming);

            Console.WriteLine("{0," + columnSpacing + "}|{1,-" + columnSpacing + "}", humanCardLabel + " ", " " + cpuCardLabel);
            UpdateCardCount();
        }

        /// <summary>
        /// Display a message centered in the columns
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="spaceChar">The character to use for empty space</param>
        static void DisplayCenteredMessage(string msg, char spaceChar = ' ')
        {
            // Make sure the message isn't too long first
            if (msg.Length >= columnSpacing * 2 + 1)
            {
                Console.WriteLine(msg);
                return;
            }

            Console.WriteLine(new string(spaceChar, columnSpacing - (int)Math.Floor(msg.Length / 2.0))
                + msg + new string(spaceChar, columnSpacing - (int)Math.Ceiling(msg.Length / 2.0)));
        }

        /// <summary>
        /// Handle a WAR (when the players' cards are tied)
        /// </summary>
        /// <param name="currentHand"></param>
        /// <param name="gameTie"></param>
        /// <returns>Returns true if the human player won, false if the CPU won or if it was a tie</returns>
        static bool HandleWar(List<Card> currentHand, ref bool gameTie)
        {
            // The cards used for comparisons
            Card humanCompareCard = null, cpuCompareCard = null;
            // The card objects to use for displaying
            Card humanDisplayCard, cpuDisplayCard;

            WarSound();

            // Continue to WAR until either a winner is determined or one or both players run out of cards
            while (true)
            {
                // Determine whether both players have cards first
                if (!humanPlayer.HasCards && !cpuPlayer.HasCards)
                {
                    // Neither player has cards, it's a draw
                    gameTie = true;
                    DisplayCenteredMessage("Both players are out of cards");
                    return false;
                }
                else if (!humanPlayer.HasCards)
                {
                    // Human player is out of cards
                    DisplayCenteredMessage(humanPlayer.PlayerName + "is out of cards");
                    return false;
                }
                else if (!cpuPlayer.HasCards)
                {
                    // CPU is out of cards
                    DisplayCenteredMessage(cpuPlayer.PlayerName + "is out of cards");
                    return true;
                }


                // Whether or not either player has drawn their final card
                bool humanFinalCard = false, cpuFinalCard = false;

                // Both players have cards, time to draw
                // Draw up to 3 cards. If a player has less than 3 cards, use their last card for the WAR check
                for (int i = 0; i < 3; i++)
                {
                    // If both have run out of cards, just break the loop
                    if (humanFinalCard && cpuFinalCard)
                        break;

                    // Draw the next cards if possible. Set the display card to null if there is no card
                    if (humanFinalCard)
                        humanDisplayCard = null;
                    else
                    {
                        humanCompareCard = humanPlayer.PlayCard();
                        humanDisplayCard = humanCompareCard;
                        currentHand.Add(humanCompareCard);

                        // If the last card was drawn (either the player is out of cards or it's the third card), set the final card variable to true
                        humanFinalCard = (i == 2 || !humanPlayer.HasCards);
                    }

                    if (cpuFinalCard)
                        cpuDisplayCard = null;
                    else
                    {
                        cpuCompareCard = cpuPlayer.PlayCard();
                        cpuDisplayCard = cpuCompareCard;
                        currentHand.Add(cpuCompareCard);

                        // If the last card was drawn (either the player is out of cards or it's the third card), set the final card variable to true
                        cpuFinalCard = (i == 2 || !cpuPlayer.HasCards);
                    }

                    // Display the cards, show them facedown if they aren't the last cards
                    DisplayCards(humanDisplayCard, cpuDisplayCard, !humanFinalCard, !cpuFinalCard);
                }

                // Decide who won the WAR
                if (humanCompareCard.GetCardValue() > cpuCompareCard.GetCardValue())
                    return true;
                else if (humanCompareCard.GetCardValue() < cpuCompareCard.GetCardValue())
                    return false;
                else
                {
                    // Add a Sleep call for timing
                    Thread.Sleep(pauseTiming);

                    // It's a tie, just display that fact and start another WAR
                    DisplayCenteredMessage("TIE", '-');
                }
            }
        }

        /// <summary>
        /// Play a tune whenever WAR is activated
        /// </summary>
        static void WarSound()
        {
            // Set the pitch and timing variables
            int beepPitch = 775;
            int beepLength = 100;
            int pauseLength = 200;

            // Use Console.Beep to play the notes and Thread.Sleep to adjust timing
            Console.Beep(beepPitch, beepLength);
            Thread.Sleep(pauseLength);
            Console.Beep(beepPitch, beepLength);
            Console.Beep(beepPitch, beepLength);
            Console.Beep(beepPitch, beepLength);
            Console.Beep(beepPitch, beepLength);
            Thread.Sleep(pauseLength + 50);
            Console.Beep(beepPitch, beepLength);
        }
    }
}
