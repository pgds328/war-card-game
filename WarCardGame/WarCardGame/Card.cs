﻿namespace WarCardGame
{
    /// <summary>
    /// Possible suits for a card
    /// </summary>
    enum CardSuit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }

    /// <summary>
    /// Possible values for a card
    /// </summary>
    // Enums are lined up with their actual values (i.e. Two = 2) so the
    // integer representation can be used for most of the labels
    enum CardValue
    {
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13,
        Ace = 14
    }

    /// <summary>
    /// Holds the value and suit of each card
    /// </summary>
    class Card
    {
        CardValue value;
        CardSuit suit;

        /// <summary>
        /// Create a card with the specified value and suit
        /// </summary>
        /// <param name="initValue">The value of the card</param>
        /// <param name="initSuit">The suit of the card</param>
        public Card(CardValue initValue, CardSuit initSuit)
        {
            value = initValue;
            suit = initSuit;
        }

        
        /// <summary>
        /// Return the integer value of the CardValue enum for comparisons
        /// </summary>
        /// <returns>The integer representation of the card value</returns>
        public int GetCardValue()
        {
            return (int)value;
        }
        
        /// <summary>
        /// Return the label for the card based on the card's value and suit
        /// </summary>
        /// <returns>The label for the card</returns>
        public string GetCardLabel()
        {
            string valueLabel, suitLabel = "";


            // Set the value label based on the CardValue enum
            if (value == CardValue.Jack || value == CardValue.Queen ||
                value == CardValue.King || value == CardValue.Ace)
            {
                // If the card is a face card or an ace, use the first letter as the label
                valueLabel = value.ToString()[0].ToString();
            }
            else
            {
                // If the card is a number card, just use the integer value as the label
                valueLabel = ((int)value).ToString();
            }


            // Set the suit label to the icon associated with the suit based on the CardSuit enum
            switch (suit)
            {
                case CardSuit.Clubs:
                    {
                        suitLabel = "\u2663";
                        break;
                    }
                case CardSuit.Diamonds:
                    {
                        suitLabel = "\u2666";
                        break;
                    }
                case CardSuit.Hearts:
                    {
                        suitLabel = "\u2665";
                        break;
                    }
                case CardSuit.Spades:
                    {
                        suitLabel = "\u2660";
                        break;
                    }
            }


            // Return the final concatenated label
            return valueLabel + suitLabel;
        }
    }
}
