﻿using System.Collections.Generic;

namespace WarCardGame
{
    /// <summary>
    /// Manages a player and their deck
    /// </summary>
    class Player
    {
        // The player's name, set in the constructor
        public string PlayerName { get; private set; }

        // The player's deck
        // Use a queue since cards are always only going to be
        // taken from the top of the deck and added to the bottom
        Queue<Card> deck = new Queue<Card>();


        public Player(string name)
        {
            PlayerName = name;
        }


        /// <summary>
        /// Adds a card to the player's deck
        /// </summary>
        /// <param name="newCard">The card to add to the deck</param>
        public void AddCard(Card newCard)
        {
            deck.Enqueue(newCard);
        }

        /// <summary>
        /// Adds a group of cards to the player's deck
        /// </summary>
        /// <param name="newCards">The list of cards to add to the deck</param>
        public void AddCards(List<Card> newCards)
        {
            // Loop through each card in the collection and add it
            foreach (Card newCard in newCards)
            {
                deck.Enqueue(newCard);
            }
        }

        /// <summary>
        /// Removes a card from the top of the player's deck and returns it
        /// </summary>
        /// <returns>The card that was just removed from the deck</returns>
        public Card PlayCard()
        {
            // Make sure there are cards available
            if (HasCards)
                return deck.Dequeue();
            else
                return null;
        }

        /// <summary>
        /// Clear the player's deck
        /// </summary>
        public void ClearDeck()
        {
            deck.Clear();
        }

        /// <summary>
        /// Return whether the player still has cards in their deck
        /// </summary>
        public bool HasCards
        {
            get
            {
                return deck.Count > 0;
            }
        }

        /// <summary>
        /// Return the amount of cards in the player's deck
        /// </summary>
        public int CardCount
        {
            get
            {
                return deck.Count;
            }
        }
    }
}
